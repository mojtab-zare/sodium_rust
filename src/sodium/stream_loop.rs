use crate::sodium::gc::Finalize;
use crate::sodium::gc::Trace;
use crate::sodium::impl_;
use crate::sodium::IsStream;
use crate::sodium::Stream;

pub struct StreamLoop<A> {
    pub impl_: impl_::StreamLoop<A>
}

impl<A: Trace + Finalize + Clone + 'static> StreamLoop<A> {
    pub fn loop_<SA: IsStream<A>>(&self, sa: SA) {
        self.impl_.loop_(sa.to_stream().impl_);
    }

    pub fn to_stream(&self) -> Stream<A> {
        Stream {
            impl_: self.impl_.to_stream()
        }
    }
}

impl<A: Trace + Finalize + Clone + 'static> Clone for StreamLoop<A> {
    fn clone(&self) -> Self {
        StreamLoop {
            impl_: self.impl_.clone()
        }
    }
}
