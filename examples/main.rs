use std::cell::RefCell;
use std::io::prelude::*;
use std::net::TcpListener;
use std::net::TcpStream;
use std::rc::Rc;

use rusty_frp::sodium::{CellLoop, IsCell};
use rusty_frp::sodium::IsStream;
use rusty_frp::sodium::SodiumCtx;
use rusty_frp::sodium::Stream;
use rusty_frp::sodium::StreamSink;

fn main() {
    run();

//    run_server();
}

fn run() {
    println!("Run!");
    let mut sodium_ctx = SodiumCtx::new();
    let sodium_ctx = &mut sodium_ctx;
    {
        let delta = sodium_ctx.new_stream_sink();


        let balance_cl = sodium_ctx.transaction(
            |sodium_ctx| {
                let mut a: CellLoop<u32> = sodium_ctx.new_cell_loop();

                let update = delta.snapshot2(&a, |a: &u32, b: &u32| a + b);
                a.loop_(update.hold(0u32));

                a
            }
        );

        let listener = balance_cl.listen(move |x| println!("{:?}", x));
        delta.send(&1u32);
        delta.send(&1u32);
        delta.send(&1u32);
        listener.unlisten();
    }
    assert_memory_freed(sodium_ctx);
}


pub fn assert_memory_freed(sodium_ctx: &mut SodiumCtx) {
    let node_count = sodium_ctx.node_count();
    if node_count != 0 {
        panic!("memory leak detected, {} nodes are remaining", node_count);
    }
}


//fn run_server() {
//    let listener = TcpListener::bind("127.0.0.1:8080").unwrap();
//
//    let mut sodium_ctx = SodiumCtx::new();
//    let sodium_ctx = &mut sodium_ctx;
//    let stream_sink = sodium_ctx.new_stream_sink();
//    let out: Rc<RefCell<Vec<String>>> = Rc::new(RefCell::new(Vec::new()));
//
//    let l = stream_sink.map(move |st: &String| {
//        st.to_lowercase()
//    })
//        .listen(
//            move |payload| {
//                println!("new connection and payload:{}", payload);
//            }
//        );
//
////        assert_eq!(vec![8], *(*out).borrow());
//
//
//    for stream in listener.incoming() {
//        let mut stream = stream.unwrap();
//        let mut buffer = [0; 512];
//        stream.read(&mut buffer).unwrap();
//
//        stream_sink.send(&String::from_utf8_lossy(&buffer[..]).to_string());
//        println!("Connection established!");
//    }
//    l.unlisten();
//
//
//    assert_memory_freed(sodium_ctx);
//}
